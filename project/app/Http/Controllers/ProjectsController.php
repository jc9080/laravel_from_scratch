<?php

namespace App\Http\Controllers;

use App\Project;
//use PHPUnit\Util\Filesystem;
//use Illuminate\Filesystem\Filesystem;

class ProjectsController extends Controller
{
    public function index()
    {
        $projects = Project::all();

        return view('projects.index', compact('projects'));

    }

    public function create()
    {
        return view('projects.create');
    }

//    public function show(Filesystem $file)
//    {
//        dd($file);
//        //return view('projects.show', compact('project'));
//    }


    public function show(Project $project)
    {
        return view('projects.show', compact('project'));
    }


    public function edit(Project $project) //responds to example.com/projects/1/edit
    {
        return view('projects.edit', compact('project'));
    }

    public function update(Project $project)
    {
        $project->update(request(['title', 'description']));

        return redirect('/projects');
    }

    public function destroy(Project $project)
    {
        $project->delete();

        return redirect('/projects');
    }

    public function store()
    {
        $attributes = request()->validate([
            'title' => ['required', 'min:3', 'max:255'],
            'description' => ['required', 'min:3', 'max:255']
            //'password' => ['required', 'confirmed']
        ]);

        Project::create($attributes);

        return redirect('/projects');
    }



}
