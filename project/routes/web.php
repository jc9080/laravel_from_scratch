<?php

//use Illuminate\Filesystem\Filesystem;

app()->singleton('App\Example', function() {

    return new \App\Example;
});

Route::get('/', function () {

    dd(app('App\Example'));

    return view('welcome');
});

Route::resource('projects', 'ProjectsController');

Route::post('projects/{project}/tasks', 'ProjectTasksController@store');
Route::post('/completed-tasks/{task}', 'CompletedTasksController@store');
Route::delete('/completed-tasks/{task}', 'CompletedTasksController@destroy');

//Route::patch('/tasks/{task}', 'ProjectTasksController@update');









/* Routing Conventions
 * GET /projects (index) : show/fetch all projects
 * GET /projects/create (create) : show a format to create a new project
 * GET /projects/1 (show) : show a single project
 * POST /projects (store) : store a new project
 * GET /projects/1/edit (edit) : show a form to edit the project
 * PATCH /projects/1 (update) : update that project
 * DELETE /projects/1 (destroy) : delete that project
 * */

//Actual Route EndPoints
//Route::get('/projects', 'ProjectsController@index');
//Route::get('/projects/create', 'ProjectsController@create');
//Route::get('/projects/{project}', 'ProjectsController@show');
//Route::post('/projects', 'ProjectsController@store');
//Route::get('/projects/{project}/edit', 'ProjectsController@edit');
//Route::patch('/projects/{project}', 'ProjectsController@update');
//Route::delete('/projects/{project}', 'ProjectsController@destroy');

